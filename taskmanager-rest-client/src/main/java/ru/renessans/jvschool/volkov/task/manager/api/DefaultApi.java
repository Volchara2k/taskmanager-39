package ru.renessans.jvschool.volkov.task.manager.api;

import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
import ru.renessans.jvschool.volkov.task.manager.invoker.ApiClient;
import ru.renessans.jvschool.volkov.task.manager.invoker.Configuration;
import ru.renessans.jvschool.volkov.task.manager.invoker.Pair;

import javax.ws.rs.core.GenericType;

import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DefaultApi {
  private ApiClient apiClient;

  public DefaultApi() {
    this(Configuration.getDefaultApiClient());
  }

  public DefaultApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Create project
   * Returns created project. Created project required.
   * @param projectDTO Created project (required)
   * @return ProjectDTO
   * @throws ApiException if fails to make API call
   */
  public ProjectDTO createProject(ProjectDTO projectDTO) throws ApiException {
    Object localVarPostBody = projectDTO;
    
    // verify the required parameter 'projectDTO' is set
    if (projectDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'projectDTO' when calling createProject");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/create".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<ProjectDTO> localVarReturnType = new GenericType<ProjectDTO>() {};
    return apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Create project
   * Returns created project. Created project required.
   * @param projectDTO Created project (required)
   * @return ProjectDTO
   * @throws ApiException if fails to make API call
   */
  public ProjectDTO createProject_0(ProjectDTO projectDTO) throws ApiException {
    Object localVarPostBody = projectDTO;
    
    // verify the required parameter 'projectDTO' is set
    if (projectDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'projectDTO' when calling createProject_0");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/create".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<ProjectDTO> localVarReturnType = new GenericType<ProjectDTO>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Create task
   * Returns created task. Created task required.
   * @param taskDTO Created project (required)
   * @return TaskDTO
   * @throws ApiException if fails to make API call
   */
  public TaskDTO createTask(TaskDTO taskDTO) throws ApiException {
    Object localVarPostBody = taskDTO;
    
    // verify the required parameter 'taskDTO' is set
    if (taskDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'taskDTO' when calling createTask");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/create".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<TaskDTO> localVarReturnType = new GenericType<TaskDTO>() {};
    return apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Create task
   * Returns created task. Created task required.
   * @param taskDTO Created project (required)
   * @return TaskDTO
   * @throws ApiException if fails to make API call
   */
  public TaskDTO createTask_0(TaskDTO taskDTO) throws ApiException {
    Object localVarPostBody = taskDTO;
    
    // verify the required parameter 'taskDTO' is set
    if (taskDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'taskDTO' when calling createTask_0");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/create".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<TaskDTO> localVarReturnType = new GenericType<TaskDTO>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Delete project by id
   * Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.
   * @param id Unique ID of project (required)
   * @return Integer
   * @throws ApiException if fails to make API call
   */
  public Integer deleteProjectById(String id) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling deleteProjectById");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/delete/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Integer> localVarReturnType = new GenericType<Integer>() {};
    return apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Delete task by id
   * Returns integer deleted flag: 1 - true, 0 - false. Unique ID required.
   * @param id Unique ID of task (required)
   * @return Integer
   * @throws ApiException if fails to make API call
   */
  public Integer deleteTaskById(String id) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling deleteTaskById");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/delete/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<Integer> localVarReturnType = new GenericType<Integer>() {};
    return apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Edit project
   * Returns edited project. Edited project required.
   * @param projectDTO Edited project (required)
   * @return ProjectDTO
   * @throws ApiException if fails to make API call
   */
  public ProjectDTO editProject(ProjectDTO projectDTO) throws ApiException {
    Object localVarPostBody = projectDTO;
    
    // verify the required parameter 'projectDTO' is set
    if (projectDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'projectDTO' when calling editProject");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/edit/".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<ProjectDTO> localVarReturnType = new GenericType<ProjectDTO>() {};
    return apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Edit project
   * Returns edited project. Edited project required.
   * @param projectDTO Edited project (required)
   * @return ProjectDTO
   * @throws ApiException if fails to make API call
   */
  public ProjectDTO editProject_0(ProjectDTO projectDTO) throws ApiException {
    Object localVarPostBody = projectDTO;
    
    // verify the required parameter 'projectDTO' is set
    if (projectDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'projectDTO' when calling editProject_0");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/edit/".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<ProjectDTO> localVarReturnType = new GenericType<ProjectDTO>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Edit task
   * Returns edited task. Edited task required.
   * @param taskDTO Edited project (required)
   * @return TaskDTO
   * @throws ApiException if fails to make API call
   */
  public TaskDTO editTask(TaskDTO taskDTO) throws ApiException {
    Object localVarPostBody = taskDTO;
    
    // verify the required parameter 'taskDTO' is set
    if (taskDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'taskDTO' when calling editTask");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/edit/".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<TaskDTO> localVarReturnType = new GenericType<TaskDTO>() {};
    return apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Edit task
   * Returns edited task. Edited task required.
   * @param taskDTO Edited project (required)
   * @return TaskDTO
   * @throws ApiException if fails to make API call
   */
  public TaskDTO editTask_0(TaskDTO taskDTO) throws ApiException {
    Object localVarPostBody = taskDTO;
    
    // verify the required parameter 'taskDTO' is set
    if (taskDTO == null) {
      throw new ApiException(400, "Missing the required parameter 'taskDTO' when calling editTask_0");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/edit/".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<TaskDTO> localVarReturnType = new GenericType<TaskDTO>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Get all projects
   * Returns a complete list of project details by order of creation.
   * @return List<ProjectDTO>
   * @throws ApiException if fails to make API call
   */
  public List<ProjectDTO> getAllProjects() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/api/projects".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ProjectDTO>> localVarReturnType = new GenericType<List<ProjectDTO>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Get all projects
   * Returns a complete list of projects details by order of creation.
   * @return List<TaskDTO>
   * @throws ApiException if fails to make API call
   */
  public List<TaskDTO> getAllTasks() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/api/tasks".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<TaskDTO>> localVarReturnType = new GenericType<List<TaskDTO>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Get project by ID
   * Returns project by unique ID. Unique ID required.
   * @param id Unique ID of project (required)
   * @return ProjectDTO
   * @throws ApiException if fails to make API call
   */
  public ProjectDTO getProjectById(String id) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling getProjectById");
    }
    
    // create path and map variables
    String localVarPath = "/api/projects/view/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<ProjectDTO> localVarReturnType = new GenericType<ProjectDTO>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Get task by ID
   * Returns task by unique ID. Unique ID required.
   * @param id Unique ID of task (required)
   * @return TaskDTO
   * @throws ApiException if fails to make API call
   */
  public TaskDTO getTaskById(String id) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling getTaskById");
    }
    
    // create path and map variables
    String localVarPath = "/api/tasks/view/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<TaskDTO> localVarReturnType = new GenericType<TaskDTO>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
