package ru.renessans.jvschool.volkov.task.manager.listener.task;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

@RequiredArgsConstructor
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    protected final TaskSoapEndpoint taskEndpoint;

}