package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpointService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpointService;

import java.util.concurrent.Executor;

public interface IEndpointConfiguration {

    @NotNull
    Executor executor();

    @NotNull
    ProjectSoapEndpointService projectEndpointService();

    @NotNull
    TaskSoapEndpointService taskEndpointService();

    @NotNull
    ProjectSoapEndpoint projectEndpoint(
            @NotNull ProjectSoapEndpointService projectEndpointService
    );

    @NotNull
    TaskSoapEndpoint taskEndpoint(
            @NotNull TaskSoapEndpointService taskEndpointService
    );

}