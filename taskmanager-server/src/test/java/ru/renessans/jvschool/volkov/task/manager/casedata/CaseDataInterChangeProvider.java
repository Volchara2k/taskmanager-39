package ru.renessans.jvschool.volkov.task.manager.casedata;

public final class CaseDataInterChangeProvider {

    @SuppressWarnings("unused")
    public Object[] invalidFilenamesCaseData() {
        return new Object[]{
                new Object[]{
                        "test",
                        null
                },
                new Object[]{
                        "string",
                        ""
                },
                new Object[]{
                        "demo",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidFilenamesClassCaseData() {
        return new Object[]{
                new Object[]{String.class, null},
                new Object[]{String.class, ""},
                new Object[]{String.class, "    "}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validClassesCaseData() {
        return new Object[]{
                new Object[]{String.class, "fg6A0Induv"},
                new Object[]{String.class, "JyWYAYmqJf"},
                new Object[]{String.class, "nnerVw24PV"}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validLinesCaseData() {
        return new Object[]{
                new Object[]{"demo", "fg6A0Induv"},
                new Object[]{".....", "JyWYAYmqJf"},
                new Object[]{"!23", "nnerVw24PV"}
        };
    }

}