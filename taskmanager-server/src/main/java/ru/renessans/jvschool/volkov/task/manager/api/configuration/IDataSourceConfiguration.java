package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

public interface IDataSourceConfiguration {

    @NotNull
    DataSource dataSource();

    @NotNull
    LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull DataSource dataSource
    );

    @NotNull
    PlatformTransactionManager transactionManager(
            @NotNull LocalContainerEntityManagerFactoryBean entityManagerFactory
    );

}