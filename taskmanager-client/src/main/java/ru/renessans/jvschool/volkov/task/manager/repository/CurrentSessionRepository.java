package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;

import java.util.Objects;

@Repository
public final class CurrentSessionRepository implements ICurrentSessionRepository {

    @Nullable
    private SessionDTO currentSessionDTO;

    @NotNull
    @Override
    public SessionDTO set(@NotNull final SessionDTO sessionDTO) {
        this.currentSessionDTO = sessionDTO;
        return sessionDTO;
    }

    @Nullable
    @Override
    public SessionDTO get() {
        return this.currentSessionDTO;
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO delete() {
        @Nullable final SessionDTO current = this.get();
        if (Objects.isNull(current)) throw new InvalidSessionException();
        this.currentSessionDTO = null;
        return current;
    }

}