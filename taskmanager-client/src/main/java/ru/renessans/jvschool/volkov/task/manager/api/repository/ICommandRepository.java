package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<String> getAllCommands();

    @NotNull
    Collection<String> getAllTerminalCommands();

    @NotNull
    Collection<String> getAllArgumentCommands();

}